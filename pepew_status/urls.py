from django.conf.urls import url

from .views import add_status, index, profile

app_name='pepew_status'
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add-status/$', add_status, name='add_status'),
    url(r'^profile/$', profile, name='profile'),
]
