from django.apps import AppConfig


class PepewStatusConfig(AppConfig):
    name = 'pepew_status'
