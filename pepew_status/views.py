from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse

from .forms import StatusForm
from .models import Status


def index(request):
    status = Status.objects.all()
    form = StatusForm()
    html = 'index.html'
    return render(request, html, {'status': status, 'form': StatusForm})


def add_status(request):
    if request.method == 'POST':
        form = StatusForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('status:index'))
    else:
        form = StatusForm()
    return render(request, 'index.html', {'form': form,
                                          'status': Status.objects.all()})


def profile(request):
    return render(request, 'profile.html')