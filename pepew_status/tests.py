import datetime
from unittest import mock
import time

import pytz

from django.http import HttpRequest
from django.test import TestCase, Client
from django.urls import resolve
from django.utils import timezone

from .forms import StatusForm
from .models import Status
from .views import add_status, index, profile
from django.test import TestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys



class PepewStatusUnitTest(TestCase):

    def test_landing_page_exists(self):
        response = Client().get('/status/')
        self.assertEqual(response.status_code, 200)

    def test_invalid_url_not_found(self):
        response = Client().get('/status/ancol/')
        self.assertEqual(response.status_code, 404)

    def test_root_url_redirects_to_index(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/status/', 302, 200)

    def test_landing_page_using_index_func(self):
        found = resolve('/status/')
        self.assertEqual(found.func, index)

    def test_landing_page_uses_template(self):
        response = Client().get('/status/')
        self.assertTemplateUsed(response, 'index.html')

    def test_landing_page_content_has_content(self):
        response = index(HttpRequest())
        html_response = response.content.decode('utf8')
        self.assertIn('Hello apa kabar?', html_response)

    def test_model_can_create_new_status(self):
        Status.objects.create(
            status='Ancol',
            created_at=timezone.now()
        )
        count_all_status = Status.objects.all().count()
        self.assertEqual(count_all_status, 1)

    def test_model_date_auto_now_add_works(self):
        mocked_time = datetime.datetime(2018, 10, 10, 1, 1, 1, tzinfo=pytz.utc)
        with mock.patch('django.utils.timezone.now', mock.Mock(return_value=mocked_time)):
            new_status = Status.objects.create()
            self.assertEqual(new_status.created_at, mocked_time)

    def test_form_status_input_has_css_classes(self):
        form = StatusForm()
        self.assertIn('class="form-control"', form.as_p())
        self.assertIn('id="id_status"', form.as_p())
        self.assertIn('rows="5"', form.as_p())
        self.assertIn('cols="8"', form.as_p())

    def test_form_validation_for_blank_status(self):
        form = StatusForm(data={
            'status': '',
        })
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status'], ['Status harus diisi.']
        )

    def test_form_validation_for_exceeding_status_length(self):
        form = StatusForm(data={
            'status': '9bmGpyUHaFUibqVKs5pZNz4f8AfcH80cco4HIobnfld1mgdmhSVhm3qDOUwHsC9vxhal6tdvbmrd3gR5K4UJxS1mA9bw2PO1IGLU3HHulltB6aFOIz5vHJjEDiKB9djeBh54hsjOVZGWjTVHNP24vD160hSEuAD7HeKLNICd6rIYeFeR1MVJh6aupB0showgZpJcE8DUDKHbPVEvK809zQr3Dzy4rR9d4rpEzA9x4Qo9ecxec9xpXyd4BbOuwUOVW32TOsKATig4qk5YJny6jDxX9NWGvomqsNb9mZfoJlFG6',
        })
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status'], ['Status tidak boleh melebihi 300 karakter.']
        )

    def test_form_validation_for_maximum_status_length(self):
        form = StatusForm(data={
            'status': '9bmGpyUHaFUibqVKs5pZNz4f8AfcH80cco4HIobnfld1mgdmhSVhm3qDOUwHsC9vxhal6tdvbmrd3gR5K4UJxS1mA9bw2PO1IGLU3HHulltB6aFOIz5vHJjEDiKB9djeBh54hsjOVZGWjTVHNP24vD160hSEuAD7HeKLNICd6rIYeFeR1MVJh6aupB0showgZpJcE8DUDKHbPVEvK809zQr3Dzy4rR9d4rpEzA9x4Qo9ecxec9xpXyd4BbOuwUOVW32TOsKATig4qk5YJny6jDxX9NWGvomqsNb9mZfoJlFG',
        })
        self.assertTrue(form.is_valid())

    def test_post_status_using_add_status_func(self):
        found = resolve('/status/add-status/')
        self.assertEqual(found.func, add_status)

    def test_get_form_function(self):
        response = Client().get('/status/add-status/')
        self.assertEqual(response.status_code, 200)

    def test_post_success_and_render_the_result(self):
        test_status = 'Ancol'
        response_post = Client().post('/status/add-status/',
                                      {
                                          'status': test_status
                                      })
        self.assertEqual(response_post.status_code, 302)
        response = Client().get('/status/')
        html_response = response.content.decode('utf8')
        self.assertIn(test_status, html_response)

    def test_post_error_and_render_the_result(self):
        test_status = 'Ancol'
        response_post = Client().post('/status/add-status/',
                                      {
                                          'status': ''
                                      })
        self.assertEqual(response_post.status_code, 200)
        response = Client().get('/status/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test_status, html_response)

    def test_post_success_and_exists_in_the_database(self):
        Client().post('/status/add-status/',
                                      {
                                          'status': 'Test'
                                      })
        status_count = Status.objects.all().count()
        self.assertEqual(status_count, 1)

    def test_post_error_and_does_not_exists_in_the_database(self):
        Client().post('/status/add-status/',
                                      {
                                          'status': ''
                                      })
        status_count = Status.objects.all().count()
        self.assertEqual(status_count, 0)

    def test_profile_page_exist(self):
        response = Client().get('/status/profile/')
        self.assertEqual(response.status_code, 200)

    def test_profile_page_using_profile_func(self):
        found = resolve('/status/profile/')
        self.assertEqual(found.func, profile)

    def test_profile_url_redirection(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/status/profile/', 302, 200)

    def test_profile_page_uses_template(self):
        response = Client().get('/status/profile/')
        self.assertTemplateUsed(response, 'profile.html')

    def test_profile_page_has_title_element(self):
        response = profile(HttpRequest())
        html_response = response.content.decode('utf8')
        self.assertIn('<title>', html_response)

    def test_profile_page_has_navbar(self):
        response = profile(HttpRequest())
        html_response = response.content.decode('utf8')
        self.assertIn('nav class="navbar navbar-expand-lg main-navbar"', html_response)
        self.assertIn('class="navbar-brand navbar-button"', html_response)

    def test_profile_page_contains_self_image_element(self):
        response = profile(HttpRequest())
        html_response = response.content.decode('utf8')
        self.assertIn('<img class="mx-auto d-block avatar-image"', html_response)


    def test_profile_page_contains_academic_div_element(self):
        response = profile(HttpRequest())
        html_response = response.content.decode('utf8')
        self.assertIn('<div class="container mx-auto d-block academic-div">', html_response)

    def test_profile_page_contains_project_div_element(self):
        response = profile(HttpRequest())
        html_response = response.content.decode('utf8')
        self.assertIn('<div class="container mx-auto d-block project-div">', html_response)

    def test_profile_page_contains_gskill_div_element(self):
        response = profile(HttpRequest())
        html_response = response.content.decode('utf8')
        self.assertIn('<div class="container mx-auto d-block skill-div">', html_response)

    def test_profile_page_contains_skill_logo_div_class(self):
        response = profile(HttpRequest())
        html_response = response.content.decode('utf8')
        self.assertIn('class="container skill-logo-div"', html_response)

# class functionalTest7(TestCase):
#     def setUp(self):
#         self.selenium = webdriver.Firefox()
#         super(functionalTest7, self).setUp()

#     def tearDown(self):
#         self.selenium.quit()
#         super(functionalTest7, self).tearDown()

#     def test_input_status_coba_coba(self):
#         selenium = self.selenium
#         selenium.get('http://localhost:8000/status/')

#         time.sleep(3)  # Makes the user actually see the page before proceeding

#         input_string = 'Coba Coba coba'
#         status_field = selenium.find_element_by_id('id_status')
#         submit_button = selenium.find_element_by_id('hehe')
#         status_field.send_keys(input_string)

#         time.sleep(1)  # Makes the user actually see the page before proceeding

#         submit_button.click()

#         time.sleep(1)  # Makes the user actually see the page before proceeding

#         status_col = selenium.find_elements_by_id('container')[-1]
#         # self.assertEqual(input_string, status_col.text)
        
#         element1 = selenium.find_element_by_tag_name('div').get_attribute('class')
#         self.assertEqual(element1, 'main-div')
#         element2 = selenium.find_element_by_tag_name('h1').get_attribute('class')
#         self.assertEqual(element2, 'page-header')
#         css1 = selenium.find_element_by_tag_name('h1').get_attribute('style')
#         self.assertEqual(css1,'color: black;')
#         css2 = selenium.find_element_by_tag_name('div').get_attribute('style')
#         self.assertEqual(css2,'color: black;')
    
# class functionalTest7(TestCase):
#     def setUp(self):
#         self.selenium = webdriver.Firefox()
#         super(functionalTest7, self).setUp()

#     def tearDown(self):
#         self.selenium.quit()
#         super(functionalTest7, self).tearDown()

#     def test_input_status_coba_coba(self):
#         selenium = self.selenium
#         selenium.get('https://story6-yaumialfadha.herokuapp.com')

#         time.sleep(3)  # Makes the user actually see the page before proceeding

#         input_string = 'Coba Coba coba'
#         status_field = selenium.find_element_by_id('id_status')
#         submit_button = selenium.find_element_by_id('hehe')
#         status_field.send_keys(input_string)

#         time.sleep(1)  # Makes the user actually see the page before proceeding

#         submit_button.click()

#         time.sleep(1)  # Makes the user actually see the page before proceeding

#         status_col = selenium.find_elements_by_id('container')[-1]
#         # self.assertEqual(input_string, status_col.text)
        
#         element1 = selenium.find_element_by_tag_name('div').get_attribute('class')
#         self.assertEqual(element1, 'main-div')
#         element2 = selenium.find_element_by_tag_name('h1').get_attribute('class')
#         self.assertEqual(element2, 'page-header')
#         css1 = selenium.find_element_by_tag_name('h1').get_attribute('style')
#         self.assertEqual(css1,'color: black;')
#         css2 = selenium.find_element_by_tag_name('div').get_attribute('style')
#         self.assertEqual(css2,'color: black;')
    
















        
